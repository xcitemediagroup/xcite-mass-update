<?php


namespace Pantheon\TerminusMassUpdate\Commands;
use Pantheon\Terminus\Commands\Site\InfoCommand;
use Pantheon\Terminus\Exceptions\TerminusException;


class xciteInfoCommand extends InfoCommand
{
    public function getInfo($site)
    {
        return $this->info($site);
    }

}