<?php


namespace Pantheon\TerminusMassUpdate\Commands;


class ExecWpCommand extends InfoUpdateCommandBase
{
    protected $termpath = '$HOME/terminus/vendor/bin/terminus';
    public function initupdateplugins($sitename,$command,$envname = '.dev') {

    }
    public function listplugins($sitename,$envname = '.dev'){
        exec($this->termpath . ' wp ' . $sitename . $envname . ' -- plugin list --format=json 2> /dev/null',$eoutput);
        foreach ($eoutput as $output) {
            if(isJson($output)){
                return json_decode($output);
            }
        }
        return('Pluigin list error');
    }
    public function getcoreverion($sitename,$command,$envname = '.dev'){

    }
    public function updateplugin($sitename,$command,$envname = '.dev') {

    }
    public function deleteplugin(){
        //TODO delete Rouge plugins worker etc
    }
    public function updatesessions(){
        //TODO GIT update MU plugin sessions
    }
    public function updateACF(){
        //TODO GIT Update ACF PRO from current version
        //TODO Check and update the DB for ACF only
    }
    public function fixwpconfigmerge(){
        //TODO Git check out wpconfig after creating a updatemerge branch in pantheon
    }
    public function backupdb(){
        //TODO excute a backup of the db mfor possible db changes
    }
    public function updateAFCdb(){
        //TODO MAYbe update the db for acf POS that it is
    }

    public function isJson($str)
    {
        json_decode($str);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}