<?php
namespace Pantheon\TerminusMassUpdate\Commands;
use Pantheon\Terminus\Commands\Site\InfoCommand;
use Pantheon\Terminus\Exceptions\TerminusException;

class HelloCommand extends InfoCommand
{


public function sayHello($site)
    {
        return $this->info($site);
    }
}