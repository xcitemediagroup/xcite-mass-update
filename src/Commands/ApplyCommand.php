<?php

namespace Pantheon\TerminusMassUpdate\Commands;

use Pantheon\Terminus\Commands\Connection;
use Pantheon\Terminus\Commands\Remote\WPCommand;
use Pantheon\Terminus\Exceptions\TerminusException;
use Pantheon\Terminus\Commands\Remote;


// @TODO: Autoloading.
require_once "MassUpdateCommandBase.php";
require_once "InfoUpdateCommandBase.php";
require_once "HelloCommand.php";

class ApplyCommand extends MassUpdateCommandBase
{
    protected $command = 'site:xcite-xcite-mass-update:apply';

    /**
     * Apply all available upstream updates to all sites.
     *
     * @authorize
     *
     * @command site:xcite-mass-update:apply
     * @aliases xcite-mass-update
     *
     * @param array $options
     * @return RowsOfFields
     * 
     * @throws TerminusException
     * @option upstream Update only sites using the given upstream
     * @option boolean $updatedb Run update.php after updating (Drupal only)
     * @option boolean $accept-upstream Attempt to automatically resolve conflicts in favor of the upstream
     * @option dry-run Don't actually apply the updates
     */
    public function applyAllUpdates($options = ['upstream' => '', 'updatedb' => false, 'accept-upstream' => false, 'dry-run' => false])
    {
        //TODO CLEAR CACHE ON LIVE AFTER UPDATE
        //TODO add option for one site rather then limiter


        $site_updates = $this->getAllSitesAndUpdates($options);


        foreach ($site_updates as $info) {
            if(!isset($count)){
                $count = 0;
            }else{
                $count = $count + 1;
                if($count >= 1){
                    $this->log()->notice($count);
                    die();
                }
            }
            $this->log()->notice($count);
            $site = $info['site'];
            $updates = $info['updates'];

            $env = $site->getEnvironments()->get('dev');
            $test = new ExecWpCommand();
            $sname = $site->getName();
            $this->log()->notice('site name ' . $sname);
            $test = $test->listplugins($sname,'envname');
            $this->log()->notice('site id ' . $site->id);
            $this->log()->notice('test result exewc class ' . $test);

            $siteinfo = new HelloCommand();
//            $siteinfo->logger = $this->logger;
//            $siteinfo->config = $this->config;
//            $siteinfo->container = $this->container;
//            $siteinfo->io = $this->io;
//            $siteinfo->input = $this->input;
//            $siteinfo->output = $this->output;
            $siteinfo->session = $this->session;
            $siteinfo->setSites($this->sites());
            $infolist = $siteinfo->sayHello($site->id);
            //$this->log()->notice($infolist['plan_name']);
            $planname = $infolist['plan_name'];
//            if($infolist['frozen'] === true){
//                $this->log()->notice('site is frozen');
//            }
            //terminus wp lakedelaronde.dev -- plugin list --format=json
            if(isset($eoutput)){
                unset($eoutput);
            }
            unset($evar);
            if($planname !== 'Sandbox') {
                //substr("Hello world",0,1)
                $termpath= '$HOME/terminus/vendor/bin/terminus';
                exec($termpath . ' wp lakedelaronde.dev -- plugin list --format=json 2> /dev/null',$eoutput);
                exec($termpath . ' wp lakedelaronde.dev -- core version 2> /dev/null',$evar);
                $eoutput = json_decode($eoutput[0],true);
                $evar = substr($evar[0],0,1);
                foreach($eoutput as $plugin) {
                    if($plugin['name'] == 'advanced-custom-fields' && $evar <= 4){
                        $this->log()->notice($site->getName() . ' ' . $plugin['status']. ' ' . $evar );
                        exec('echo ' . $evar . $site->getName() . ' ' . $plugin['status'] .' >> $HOME/acflogs/listofmanualsites2.csv');
                        //CHECK FOR REPEATER
                    }
                }

            }








            if ($env->get('connection_mode') !== 'git') { // @TODO Force git switch
                $this->log()->warning(
                    'Cannot apply updates to {site} because the dev environment is not in git mode.',
                    ['site' => $site->getName()]
                );
                $connectionsetter = new Connection\SetCommand();
                $connectionsetter->logger = $this->logger;
                $connectionsetter->config = $this->config;
                $connectionsetter->container = $this->container;
                //$connectionsetter->io = $this->io;
                $connectionsetter->input = $this->input;
                //$connectionsetter->output = $this->output;
                $connectionsetter->session = $this->session;
                $connectionsetter->setSites($this->sites());
                $connectionsetter->connectionSet($site->getName() . '.dev','git');
                $this->log()->warning('connection was set to git');
            }
            else {
                $logname = $options['dry-run'] ? 'DRY RUN' : 'notice';
                $this->log()->notice(
                    'Applying {updates} updates to {site}',
                    ['site' => $site->getName(), 'updates' => count($updates), 'name' => $logname]);

                // Do the actual updates if we're not in dry-run mode
                if (!$options['dry-run']) {

                    // @TODO Check for wp-config conflict and force the the upstream accept in a new branch and then checkout wp-config.php from master and push
                    // @TODO Clear cache
                    $workflow = $env->applyUpstreamUpdates(
                        isset($options['updatedb']) ? $options['updatedb'] : false,
                        isset($options['accept-upstream']) ? $options['accept-upstream'] : false
                    );
                    while (!$workflow->checkProgress()) {
                        // @TODO: Add Symfony progress bar to indicate that something is happening.
                    }
                    $this->log()->notice($workflow->getMessage());
                    //@TODO PUSH TO TEST THEN LIVE
                }
            }
        }
    }
    public function createupdatetest(){
        //TODO make the update test branch on merge conflict fail
        //TODO call fixwpconfigmerge
        //TODO try the upstream merge again on fail write to log and skip this site skip plugins as well
        //TODO FLAG WITH TAG NOT UPDATED
    }
}
