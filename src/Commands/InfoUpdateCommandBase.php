<?php


namespace Pantheon\TerminusMassUpdate\Commands;
use Pantheon\Terminus\Commands\Remote\WPCommand;
use Pantheon\Terminus\Commands\Site\InfoCommand;
//use Pantheon\Terminus\Commands\Remote\SSHBaseCommand;
use Pantheon\Terminus\Commands\Site\SiteCommand;
use Pantheon\Terminus\Commands\TerminusCommand;
use Pantheon\Terminus\Exceptions\TerminusException;
use Pantheon\TerminusMassUpdate\Commands\ExecWpCommand;


class InfoUpdateCommandBase extends SiteCommand
{
    protected $command = '';
    protected $env;

    public function __construct()
    {

        
    }
    /**
     * @return string
     */
    public function getInfoBase($site)
    {

        //$testsite= $this->getSite($site);

        $test = $this->getPropertyList($this->sites->get($site));
        //$test = $this->info($testsite);
        //gettype()
        $this->log()->notice(gettype($test));

        foreach($test as $key => $value){
            $this->log()->notice(gettype($key) . " " . gettype($value));
        }
        $this->log()->notice($test['plan_name']);
        return $test;
    }




    private function run($command, $return_raw = false)
    {
        //TODO Remove MOVED to exec commands
        //$result = $this->env->sendCommandViaSsh($command);
        $test = new WPCommand();
        $result = $test->wpCommand('amistrategies.dev',['wp plugin list']);


        return ( $return_raw ? $result : $this->parse($result) );
    }

    private function parse($str)
    {
        //TODO REMOVE MOVED TO Exec Commands
        return json_decode($str['output']);
    }

    public function get_plugin_list()
    {
        //TODO REMOVE

        $fields = array(
            'name',
            'status',
            'version',
            'update',
            'update_version',
            'update_package',
        );

        $command = 'wp plugin list --format=json --fields=' . implode(',', $fields);

        return $this->run($command);
    }
}